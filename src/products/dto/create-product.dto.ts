import { IsNotEmpty, Length, IsPositive, Min } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 64)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  @Min(5)
  price: number;
}
